// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'es6-promise/auto'
import 'weakmap'
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Vuefire from 'vuefire'
import {VueMasonryPlugin} from 'vue-masonry'
import App from './App'
import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'
import Firebase from 'firebase'
import { config } from '../firebaseConfig'
import firebaseui from 'firebaseui'
import 'firebaseui/dist/firebaseui.css'
import MuseUI from 'muse-ui'
import 'muse-ui/dist/muse-ui.css'
import './muse-ui-theme.less'

// import CMS from 'netlify-cms'

// Now the registry is available via the CMS object.
// CMS.registerPreviewTemplate('my-template', MyTemplate)

Vue.use(Vuetify)
Vue.use(Vuefire)
Vue.use(VueMasonryPlugin)
Vue.use(MuseUI)

Vue.config.productionTip = false

// Sync the router with the vuex store. This registers `store.state.route`
sync(store, router)

let app
let fbApp = Firebase.initializeApp(config)
let database = fbApp.database()
let auth = Firebase.auth()
let ui = new firebaseui.auth.AuthUI(auth)

auth.onAuthStateChanged(function (user) {
  if (!app) {
    /* eslint-disable */
    app = new Vue({
      el: '#app',
      router,
      store,
      data: {
        database,
        auth,
        ui
      },
      template: '<App :database="database" :auth="auth" :ui="ui" />',
      components: { App }
    })
  }
})
