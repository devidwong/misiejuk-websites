import firebase from 'firebase'
import firebaseui from 'firebaseui'
import 'firebaseui/dist/firebaseui.css'
import store from './store'

firebase.initializeApp({
  apiKey: 'AIzaSyBZ4Gu_5Ni2cfqHlhELgx5WGm7pBoXOHCU',
  authDomain: 'wongbackpress.firebaseapp.com',
  databaseURL: 'https://wongbackpress.firebaseio.com',
  projectId: 'wongbackpress',
  storageBucket: 'wongbackpress.appspot.com',
  messagingSenderId: '857255224900'
})

/*if (__DEV__) {
  window.firebase = firebase
}*/

export const ui = new firebaseui.auth.AuthUI(firebase.auth())

/**
 * Sync store.state.user with firebase.auth().currentUser
 *
 * This callback is called when firebase.auth() detects user changes,
 * so just update the vuex store with the new user object.
 */
firebase.auth().onAuthStateChanged(user => {
  store.commit('UPDATE_USER', user)
})
