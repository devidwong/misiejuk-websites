import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'
import Meta from 'vue-meta'

import Hello from '@/components/Hello'
import About from '@/components/About'
import Blog from '@/components/Blog'
import Article from '@/components/Article'
import Account from '@/components/Account'
import Gallery from '@/components/Gallery'
import CalendarMain from '@/components/CalendarMain'
import LoginUI from '@/components/Login-ui'

import firebase from 'firebase'

Vue.use(Router)
Vue.use(Meta)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      component: LoginUI,
      beforeEnter: (to, from, next) => {
        if (store.state.user) {
          next(from)
        } else {
          next()
        }
      }
    },
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/blog',
      name: 'Blog',
      component: Blog
    },
    {
      path: '/blog/:id',
      name: 'Article',
      props: true,
      component: Article
    },
    {
      path: '/account',
      name: 'Account',
      component: Account,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/gallery',
      name: 'Gallery',
      component: Gallery
    },
    {
      path: '/calendar',
      name: 'Calendar',
      component: CalendarMain
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  // console.log(currentUser)
  // console.log(requiresAuth)
  if (requiresAuth && !currentUser) next('Hello')
  else next()
})
export default router
