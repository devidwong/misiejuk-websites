---
title: Post zerowy
description: Sprawdzamy jak wyświetlają się style
---

## Zaczynijmy od H2
potem tekst

i potem kod
```html
<div class="container">
  <div class="main">
    <div class="article insert-wp-tags-here">
      <h1>Title</h1>
      <div class="article-content">
        <p class="intro">Intro Text</p>
        <p></p>
      </div>
      <div class="article-meta"></div>
    </div>
  </div>
</div>
```

A teraz obrazek

![alt text](/static/img/icons/android-chrome-192x192.png "testing")
