# misiejuk-websites

> Project of my full-featured website

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

Code for [misiej.uk](https://misiej.uk) blog

Base source comes from [PWA Vue template](https://github.com/vuejs-templates/pwa), modified in great article [Rise of the Butt-less website](https://css-tricks.com/the-rise-of-the-butt-less-website/) by [Evan Payne](https://evanpayne.com/).
This article explains how to make your own blog-aware Static Site Generator, prerendered from Markup and packed-up by Webpack.

Since Netlify offers an super-easy prerendering tool (still beta), PrerenderSpaPlugin has been removed. Also changed ESlint from sick Airbnb to standard version.

## Added:
[Vuetify Material Design Component Framework](https://vuetifyjs.com/en/)

Netlify CMS Integration v2

Firebase Realtime Database and Authentication

Snipcart to archive e-commerce functionality

[Vue-meta](https://github.com/declandewet/vue-meta) for SEO

Masonry.js for gallery grid

To-do list as example firebase CRUD

Calendar using [Vue-Fullcalendar](https://github.com/CroudTech/vue-fullcalendar)

## to Add:
VueX for centralized events

[Vue-filters](https://github.com/freearhey/vue2-filters) for products and blog filtering

Media preview in Netlify CMS (thumbnails suppport)

[Vue-disqus](https://github.com/ktquez/vue-disqus) for comments support

## Later:

SMS notification as AWS Lambda testing

Payments

Subdomain tests

Nested and mutiple router-views

Lazy skeleton loading

Custom Netlify CMS widgets for eg. Maps

## Our mention is to never use any kind of Server
